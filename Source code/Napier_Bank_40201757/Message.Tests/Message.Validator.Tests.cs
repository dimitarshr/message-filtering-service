﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Napier_Bank_40201757.Messages;
using Napier_Bank_40201757.Services;

namespace Napier_Bank_Messaging_Service.Tests
{
    [TestClass]
    public class MessageTestsForValidating
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_SMSIsGivenInvalidMessageBody()
        {
            SMSMessage message = new SMSMessage
            {
                Sender = "+44774344728",
                MessageBodyRaw = ""
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_SMSIsGivenInvalidSender()
        {
            SMSMessage message = new SMSMessage
            {
                Sender = "",
                MessageBodyRaw = "This is message body"
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_TweetIsGivenInvalidMessageBody()
        {
            Tweets tweet = new Tweets
            {
                Sender = "@Test",
                MessageBodyRaw = ""
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_TweetIsGivenInvalidSender()
        {
            Tweets tweet = new Tweets
            {
                Sender = "",
                MessageBodyRaw = "Greetings from @Scotland #holiday"
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_StandardEmailIsGivenInvalidSender()
        {
            EmailMessages email = new EmailMessages
            {
                Sender = "",
                Subject = "Application",
                MessageBodyRaw = "Dear Sir/Madam, My name is.."
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_StandardEmailIsGivenInvalidSubject()
        {
            EmailMessages email = new EmailMessages
            {
                Sender = "test@napier.ac.uk",
                Subject = "",
                MessageBodyRaw = "Dear Sir/Madam, My name is.."
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_StandardEmailIsGivenInvalidMessageBody()
        {
            EmailMessages email = new EmailMessages
            {
                Sender = "test@napier.ac.uk",
                Subject = "Application",
                MessageBodyRaw = ""
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_SIRIsGivenInvalidSender()
        {
            SignificantIncidentReports email = new SignificantIncidentReports
            {
                Sender = "",
                SortCode = "11-22-33",
                Incident = SignificantIncidentReports.natureOfIncident.Intelligence,
                Subject = "SIR 12/12/2018",
                MessageBodyRaw = "Dear Sir/Madam, My name is.."
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_SIRIsGivenInvalidSortCode()
        {
            SignificantIncidentReports email = new SignificantIncidentReports
            {
                Sender = "test@napier.ac.uk",
                SortCode = "",
                Incident = SignificantIncidentReports.natureOfIncident.Intelligence,
                Subject = "SIR 12/12/2018",
                MessageBodyRaw = "Dear Sir/Madam, My name is.."
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_SIRIsGivenInvalidNatureOfIncident()
        {
            SignificantIncidentReports email = new SignificantIncidentReports
            {
                Sender = "test@napier.ac.uk",
                SortCode = "11-22-33",
                Incident = SignificantIncidentReports.natureOfIncident.None,
                Subject = "SIR 12/12/2018",
                MessageBodyRaw = "Dear Sir/Madam, My name is.."
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_SIRIsGivenInvalidSubject()
        {
            SignificantIncidentReports email = new SignificantIncidentReports
            {
                Sender = "test@napier.ac.uk",
                SortCode = "11-22-33",
                Incident = SignificantIncidentReports.natureOfIncident.Intelligence,
                Subject = "",
                MessageBodyRaw = "Dear Sir/Madam, My name is.."
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowExpection_When_SIRIsGivenInvalidMessageBody()
        {
            SignificantIncidentReports email = new SignificantIncidentReports
            {
                Sender = "test@napier.ac.uk",
                SortCode = "11-22-33",
                Incident = SignificantIncidentReports.natureOfIncident.Intelligence,
                Subject = "SIR 12/12/2018",
                MessageBodyRaw = ""
            };
        }

        [TestMethod]
        public void Should_ValidateAsFalse_When_SMSIsGivenInvalidMessageBody()
        {
            Service service = Service.Instance;
            // Required to set the correct error messages.
            service.addUIConnection(new Napier_Bank_40201757.MainWindow());

            Napier_Bank_40201757.Messages.Message message = new SMSMessage();
            // The input format is 'Message Type', 'Sender' and 'Message Body' is empty.
            // This result in the validation failure.
            string input = "SMS¡+44774344728¡";

            bool actualResult = service.validateInput(message, input.Split('¡'));
            bool expectedResult = false;

            Assert.AreEqual(actualResult, expectedResult);
        }

        [TestMethod]
        public void Should_ValidateAsFalse_When_SMSIsGivenInvalidSender()
        {
            Service service = Service.Instance;
            // Required to set the correct error messages.
            service.addUIConnection(new Napier_Bank_40201757.MainWindow());

            Napier_Bank_40201757.Messages.Message message = new SMSMessage();
            // The input format is 'Message Type', 'Sender' and 'Message Body'.
            // The 'Sender' is invalid as it is missing the '+'.
            // This result in the validation failure.
            string input = "SMS¡44774344728¡Hello, this is a SMS message";

            bool actualResult = service.validateInput(message, input.Split('¡'));
            bool expectedResult = false;

            Assert.AreEqual(actualResult, expectedResult);
        }

        [TestMethod]
        public void Should_ValidateAsFalse_When_TweetIsGivenInvalidSender()
        {
            Service service = Service.Instance;
            // Required to set the correct error messages.
            service.addUIConnection(new Napier_Bank_40201757.MainWindow());

            Napier_Bank_40201757.Messages.Message tweet = new Tweets();
            // The input format is 'Message Type', 'Sender' and 'Message Body'.
            // The 'Sender' is invalid as it is missing the '@'.
            // This result in the validation failure.
            string input = "Tweet¡Test¡This is a test tweet.";

            bool actualResult = service.validateInput(tweet, input.Split('¡'));
            bool expectedResult = false;

            Assert.AreEqual(actualResult, expectedResult);
        }
    }
}
