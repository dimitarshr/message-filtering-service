﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Napier_Bank_40201757.Services;
using Napier_Bank_40201757.Messages;
using System;

namespace Napier_Bank_Messaging_Service.Tests
{
    [TestClass]
    public class MessageTestsForCategorising
    {
        [TestMethod]
        public void Should_CreateSMS_When_TheInputStringContainsSMSInTheMessageHeader()
        {
            Service service = Service.Instance;
            Napier_Bank_40201757.Messages.Message message = null;
            // The input format is 'Message Type', 'Sender' and 'Message Body'.
            string input = "SMS¡+44774344728¡Hello, this is a SMS message!";

            service.categoriseAndValidate(ref message, input);

            Assert.AreEqual(message.GetType(), typeof(SMSMessage));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_SMSStringIsMissingBody()
        {
            Service service = Service.Instance;
            service.addUIConnection(new Napier_Bank_40201757.MainWindow());
            Napier_Bank_40201757.Messages.Message message = null;
            // The input format is 'Message Type', 'Sender' and 'Message Body'.
            string input = "SMS¡+44774344728¡";

            bool expectedResult = false;
            bool actualResult = service.categoriseAndValidate(ref message, input);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void Should_CreateTweet_When_TheInputStringContainsTweetInTheMessageHeader()
        {
            Service service = Service.Instance;
            Napier_Bank_40201757.Messages.Message message = null;
            // The input format is 'Message Type', 'Sender' and 'Message Body'.
            string input = "Tweet¡@Peter¡Hello, this is a Tweet!";

            service.categoriseAndValidate(ref message, input);

            Assert.AreEqual(message.GetType(), typeof(Tweets));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_TweetStringIsMissingBody()
        {
            Service service = Service.Instance;
            service.addUIConnection(new Napier_Bank_40201757.MainWindow());
            Napier_Bank_40201757.Messages.Message message = null;
            // The input format is 'Message Type', 'Sender' and 'Message Body'.
            string input = "Tweet¡@Peter¡";

            bool expectedResult = false;
            bool actualResult = service.categoriseAndValidate(ref message, input);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void Should_CreateStandardEmail_When_TheInputStringContainsStandardEmailInTheMessageHeader()
        {
            Service service = Service.Instance;
            Napier_Bank_40201757.Messages.Message message = null;
            // The input format is 'Message Type', 'Sender', 'Subject' and 'Message Body'.
            string input = "Standard_email_message¡test@napier.ac.uk¡Subject is written¡Hello, this is a standard email!";

            service.categoriseAndValidate(ref message, input);

            Assert.AreEqual(message.GetType(), typeof(EmailMessages));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_StandardEmailIsMissingSender()
        {
            Service service = Service.Instance;
            service.addUIConnection(new Napier_Bank_40201757.MainWindow());
            Napier_Bank_40201757.Messages.Message message = null;
            // The input format is 'Message Type', 'Sender', 'Subject' and 'Message Body'.
            string input = "Standard_email_message¡¡Subject is written¡Hello, this is a standard email!";

            bool expectedResult = false;
            bool actualResult = service.categoriseAndValidate(ref message, input);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void Should_CreateSIR_When_TheInputStringContainsSIRInTheMessageHeader()
        {
            Service service = Service.Instance;
            Napier_Bank_40201757.Messages.Message message = null;
            // The input format is 'Message Type', 'Sender', 'Subject', 'Sort Code', 'Nature of Incident' and 'Message Body'.
            string input = "Significant_incident_report¡test@napier.ac.uk¡SIR 12/12/2018¡11-22-33¡Theft¡Hello, this is a SIR email!";

            service.categoriseAndValidate(ref message, input);

            Assert.AreEqual(message.GetType(), typeof(SignificantIncidentReports));
        }

        [TestMethod]
        public void Should_ReturnFalse_When_SIRIsMissingSortCode()
        {
            Service service = Service.Instance;
            service.addUIConnection(new Napier_Bank_40201757.MainWindow());
            Napier_Bank_40201757.Messages.Message message = null;
            // The input format is 'Message Type', 'Sender', 'Subject', 'Sort Code', 'Nature of Incident' and 'Message Body'.
            string input = "Significant_incident_report¡test@napier.ac.uk¡SIR 12/12/2018¡¡Theft¡Hello, this is a SIR email!";

            bool expectedResult = false;
            bool actualResult = service.categoriseAndValidate(ref message, input);

            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
