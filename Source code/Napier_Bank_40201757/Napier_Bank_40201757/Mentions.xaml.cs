﻿/*
 * Author: 40201757
 * Description: The Mentions class is part of the GUI.
 *              From here the user can see all mentions from tweets.
 * Date last modified: 12/11/2018
 */
using System;
using System.Collections.Generic;
using System.Windows;
using Napier_Bank_40201757.Services;

namespace Napier_Bank_40201757
{
    public partial class Mentions : Window
    {
        private Service service = Service.Instance;
        public Mentions()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // When the window is loaded, all mentions are read and written to the data grid.
            foreach (KeyValuePair<string, List<string>> m in service.AllMentions)
            {
                foreach (string messageID in m.Value)
                {
                    allMentionsGrid.Items.Add(new Tuple<string, string>(m.Key, messageID));
                }
            }
        }
    }
}
