﻿/*
 * Author: 40201757
 * Description: The History class is part of the GUI.
 *              From here the user can see all the messages that have been entered in the system. They are read from a JSON file.
 * Date last modified: 12/11/2018
 */
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Napier_Bank_40201757.Messages;
using Napier_Bank_40201757.Services;

namespace Napier_Bank_40201757
{
    public partial class History : Window
    {
        private Service service = Service.Instance;

        public History()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // When the window is loaded, all messages are read and written to the data grid.
            foreach (KeyValuePair<string, Message> m in service.AllMessages)
            {
                if (m.GetType() == typeof(EmailMessages))
                {
                    allMessagesGrid.Items.Add(((EmailMessages)m.Value));
                }
                else if(m.GetType() == typeof(SignificantIncidentReports))
                {
                    allMessagesGrid.Items.Add(((SignificantIncidentReports)m.Value));
                }
                else
                {
                    allMessagesGrid.Items.Add(m.Value);
                }
            }
        }

        private void allMessagesGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // In case of a double click, the selected message is displayed in a message box.
            if (allMessagesGrid.SelectedItem == null) return;
            var selectedMessage = allMessagesGrid.SelectedItem as Message;
            MessageBox.Show(selectedMessage.ToString());
        }
    }
}
