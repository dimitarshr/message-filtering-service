﻿/*
 * Author: 40201757
 * Description: The Tweets class consists of all the associated properties required to store data for a tweet.
 *              It inherits part of its attributes and properties from the Message class.
 * Date last modified: 12/11/2018
 */
using System;

namespace Napier_Bank_40201757.Messages
{
    public class Tweets : Message
    {

        public Tweets() : base()
        {
            this.messageID = "T" + messageCounter.ToString().PadLeft(9, '0');
        }

        public override string MessageID
        {
            get
            {
                return messageID;
            }
        }

        // This property return a value from a message type enum.
        public override Message.messageType MessageType
        {
            get
            {
                return messageType.Tweet;
            }
        }

        public override string Sender
        {
            get
            {
                return sender;
            }
            set
            {
                // The limit is 16, so that it excludes the '@' symbol.
                if (value.Trim().Length == 0 || value.Trim().Length > 16)
                {
                    throw new ArgumentException("* The Tweet sender should be between 1 and 15 characters long.");
                }
                if (value[0] != '@')
                {
                    throw new ArgumentException("* The Tweet sender should start with the '@' sign.");
                }
                sender = value;
            }
        }

        public override string MessageBodyRaw
        {
            get
            {
                return messageTextRaw;
            }
            set
            {
                if (value.Trim().Length == 0 || value.Trim().Length > 140)
                {
                    throw new ArgumentException("* The Tweet message body should be between 1 and 140 characters long.");
                }
                messageTextRaw = value;
            }
        }

        public override string MessageBody
        {
            get
            {
                return messageText;
            }
            set
            {
                messageText = value;
            }
        }

        // Overriding the ToString() method.
        public override string ToString()
        {
            return "ID: " + this.MessageID + "\nMessage Type: " + this.MessageType + "\n" + "Sender: " + this.Sender+ "\n" + "Body: " + this.MessageBody;
        }
    }
}
