﻿/*
 * Author: 40201757
 * Description: The EmailMessages class consists of all the associated properties required to store data for an email.
 *              It inherits part of its attributes and properties from the Message class.
 *              The 'JsonProperty(Order=..)' part of each property helps for the order of text when written to a JSON file.
 * Date last modified: 12/11/2018
 */
using System;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace Napier_Bank_40201757.Messages
{
    public class EmailMessages : Message
    {
        protected string subject;

        public EmailMessages() : base()
        {
            this.messageID = "E" + messageCounter.ToString().PadLeft(9, '0');
        }

        [JsonProperty(Order = 1)]
        public override string MessageID
        {
            get
            {
                return this.messageID;
            }
        }

        // This property return a value from a message type enum.
        [JsonProperty(Order = 2)]
        public override Message.messageType MessageType
        {
            get
            {
                return messageType.Standard_email_message;
            }
        }

        [JsonProperty(Order = 3)]
        public override string Sender
        {
            get
            {
                return this.sender;
            }
            set
            {
                if (value.Trim().Length == 0)
                {
                    throw new ArgumentException("* The Email sender is not provided.");
                }
                // This regex checks if the email is in a valid format, e.g. 'test@napier.ac.uk'
                if (!Regex.Match(value.Trim(), @"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$").Success)
                {
                    throw new ArgumentException("* The Email address is invalid.");
                }
                this.sender = value.Trim();
            }
        }

        [JsonProperty(Order = 4)]
        public string Subject
        {
            get
            {
                return this.subject;
            }
            set
            {
                if (value.Trim().Length == 0 || value.Trim().Length > 20)
                {
                    throw new ArgumentException("* The Email subject should be between 1 and 20 characters long.");
                }
                this.subject = value;
            }
        }
        [JsonProperty(Order = 7)]
        public override string MessageBodyRaw
        {
            get
            {
                return this.messageTextRaw;
            }
            set
            {
                if (value.Trim().Length == 0 || value.Trim().Length > 1028)
                {
                    throw new ArgumentException("* The Email body should be between 1 and 1028 characters long.");
                }
                this.messageTextRaw = value;
            }
        }

        [JsonProperty(Order = 8)]
        public override string MessageBody
        {
            get
            {
                return messageText;
            }
            set
            {
                messageText = value;
            }
        }

        // Overriding the ToString() method.
        public override string ToString()
        {
            return "ID: " + this.MessageID + "\nMessage Type: " + this.MessageType +"\n" + "Sender: " + this.Sender + "\n" + "Subject: " + this.Subject + "\n" + "Body: " + this.MessageBody;
        }
    }
}
