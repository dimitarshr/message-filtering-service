﻿/*
 * Author: 40201757
 * Description: The SMSMessage class consists of all the associated properties required to store data for an SMS.
 *              It inherits part of its attributes and properties from the Message class.
 * Date last modified: 12/11/2018
 */
using System;
using System.Text.RegularExpressions;

namespace Napier_Bank_40201757.Messages
{
    public class SMSMessage : Message
    {
        public SMSMessage() : base()
        {
            this.messageID = "S" + messageCounter.ToString().PadLeft(9, '0');
        }

        public override string MessageID
        {
            get
            {
                return messageID;
            }
        }

        // This property return a value from a message type enum.
        public override Message.messageType MessageType
        {
            get
            {
                return messageType.SMS;
            }
        }

        public override string Sender {
            get
            {
                return sender;
            }
            set
            {
                if (value.Trim().Length < 5 || value.Trim().Length > 15)
                {
                    throw new ArgumentException("* The SMS sender should be a numeric value between 5 and 15 digits long.");
                }
                if (!Regex.Match(value.Trim(), @"^(\+[0-9]+)$").Success)
                {
                    throw new ArgumentException("* The SMS sender phone number is invalid. It should be a numberic value \n starting with a '+'.");
                }
                sender = value;
            }
        }

        public override string MessageBodyRaw
        {
            get
            {
                return messageTextRaw;
            }
            set
            {
                if (value.Trim().Length == 0 || value.Trim().Length > 140)
                {
                    throw new ArgumentException("* The SMS body should be between 1 and 140 characters long.");
                }
                messageTextRaw = value;
            }
        }

        public override string MessageBody
        {
            get
            {
                return messageText;
            }
            set
            {
                messageText = value;
            }
        }

        // Overriding the ToString() method.
        public override string ToString()
        {
            return "ID: " + this.MessageID + "\nMessage Type: " + this.MessageType + "\n" + "Sender: " + this.Sender + "\n" + "Body: " + this.MessageBody;
        }
    }
}
