﻿/*
 * Author: 40201757
 * Description: The Message class consists of the basic properties required to store data for a message.
 *              It is inherited by the EmailMessages, SignificantReports, SMSMessage and Tweet classes.
 * Date last modified: 12/11/2018
 */

namespace Napier_Bank_40201757.Messages
{
    public abstract class Message
    {
        protected static int messageCounter = 0;
        protected string messageID;
        protected string messageTextRaw;
        protected string messageText;
        protected string sender;

        public abstract string MessageID { get; }
        public abstract messageType MessageType { get; }
        public abstract string MessageBodyRaw { get; set; }
        public abstract string MessageBody { get; set; }
        public abstract string Sender { get; set; }

        public enum messageType
        {
            SMS,
            Standard_email_message,
            Significant_incident_report,
            Tweet
        }

        public Message()
        {
            messageCounter += 1;
        }

        public static int MessageCounter
        {
            get
            {
                return messageCounter;
            }
            set
            {
                messageCounter = value;
            }
        }
    }
}
