﻿/*
 * Author: 40201757
 * Description: The SignificantIncidentReport class consists of all the associated properties required to store data for a significant incident report.
 *              It inherits part of its attributes and properties from the EmailMessages class.
 *              The 'JsonProperty(Order=..)' part of each property helps for the order of text when written to a JSON file.
 * Date last modified: 12/11/2018
 */
using System;
using Newtonsoft.Json;

namespace Napier_Bank_40201757.Messages
{
    public class SignificantIncidentReports : EmailMessages
    {
        private string sortCode;
        private natureOfIncident incident;
        public enum natureOfIncident
        {
            None,
            Theft,
            Staff_Attack,
            ATM_Theft,
            Raid,
            Customer_Attack,
            Staff_Abuse,
            Bomb_Threat,
            Terrorism,
            Suspicious_Incident,
            Intelligence,
            Cash_Loss
        }

        public SignificantIncidentReports() : base()
        {
            
        }

        // This property return a value from a message type enum.
        [JsonProperty(Order = 2)]
        public override Message.messageType MessageType
        {
            get
            {
                return messageType.Significant_incident_report;
            }
        }

        [JsonProperty(Order = 4)]
        public new string Subject
        {
            get
            {
                return this.subject;
            }
            set
            {
                if (value.Trim().Length == 0 || value.Trim().Length > 20)
                {
                    throw new ArgumentException("* The Email subject should be between 1 and 20 characters long.");
                }

                // This part validates that the subject is correct, e.g. SIR 28/11/2018.
                if (value.Substring(0, value.Length >= 3 ? 3: value.Length) != "SIR" 
                    || !DateTime.TryParse(value.Substring(value.Length > 3 ? 3 : 0, value.Length-3).Trim(), out DateTime c)
                    || value.IndexOf(' ') == -1)
                {
                    throw new ArgumentException("* The subject should be in the form of \"SIR dd/mm/yy\".");
                }
                this.subject = value;
            }
        }

        [JsonProperty(Order = 5)]
        public string SortCode
        {
            get
            {
                return this.sortCode;
            }
            set
            {
                if (value.Trim().Length < 8 || value.Trim().Length > 8 || value.Trim().Length == 0)
                {
                    throw new ArgumentException("* The sort code should be 8 digits long separated by \"-\".");
                }
                if (value.Trim()[2] != '-' || value.Trim()[5] != '-' 
                    || !int.TryParse(value.Substring(0, 2), out int a)
                    || !int.TryParse(value.Substring(3, 2), out int b)
                    || !int.TryParse(value.Substring(6, 2), out int c))
                {
                    throw new ArgumentException("* The sort code should be digits in the form of \"99-99-99\".");
                }
                this.sortCode = value;
            }
        }

        // This property return a value from a nature of incident enum.
        [JsonProperty(Order = 6)]
        public natureOfIncident Incident
        {
            get
            {
                return this.incident;
            }
            set
            {
                if (value == natureOfIncident.None)
                {
                    throw new ArgumentException("* The nature of incident cannot be \"None\".");
                }
                this.incident = value;
            }
        }

        [JsonProperty(Order = 7)]
        public override string MessageBodyRaw
        {
            get
            {
                return this.messageTextRaw;
            }
            set
            {
                if (value.Trim().Length == 0 || value.Trim().Length > 1028)
                {
                    throw new ArgumentException("* The Email body should be between 1 and 1028 characters long.");
                }
                this.messageTextRaw = value;
            }
        }

        [JsonProperty(Order = 8)]
        public override string MessageBody
        {
            get
            {
                return messageText;
            }
            set
            {
                messageText = value;
            }
        }

        // Overriding the ToString() method.
        public override string ToString()
        {
            return "ID: " + this.MessageID + "\nMessage Type: " + this.MessageType + "\n" + "Sender: " + this.Sender + "\n" + "Subject: " + this.Subject + "\n" + "Sort Code: " + this.SortCode + "\n" + "Incident: " + this.Incident + "\n" + "Body: " + this.MessageBody;
        }
    }
}
