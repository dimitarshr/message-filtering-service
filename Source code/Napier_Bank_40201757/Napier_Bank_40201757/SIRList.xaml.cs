﻿/*
 * Author: 40201757
 * Description: The SIRList class is part of the GUI.
 *              From here the user can see all SIR items from emails.
 * Date last modified: 12/11/2018
 */
using System.Collections.Generic;
using System.Windows;
using Napier_Bank_40201757.Messages;
using Napier_Bank_40201757.Services;

namespace Napier_Bank_40201757
{
    public partial class SIRList : Window
    {
        private Service service = Service.Instance;

        public SIRList()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // When the window is loaded, all SIR items are read and written to the data grid.
            foreach (KeyValuePair<string, Message> m in service.AllSIRs)
            {
                allSIRGrid.Items.Add(m.Value);
            }
        }
    }
}
