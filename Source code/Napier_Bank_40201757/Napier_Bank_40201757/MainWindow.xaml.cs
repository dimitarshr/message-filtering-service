﻿/*
 * Author: 40201757
 * Description: The MainWindow class is part of the GUI.
 *              This is the starting window.
 *              From here we can go to the History, Mentions, QuarantineList and SIRList windows.
 * Date last modified: 12/11/2018
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using Napier_Bank_40201757.Messages;
using Napier_Bank_40201757.Services;

namespace Napier_Bank_40201757
{
    public partial class MainWindow : Window
    {
        private Service service = Service.Instance;
        bool messageFromInputFile = false;
        int messageFromInputFileCounter = 0;

        public MainWindow()
        {
            InitializeComponent();

            fillInDropbox();

            hideErrorLabels();
            hideInputFields();

            service.addUIConnection(this);

            service.loadJSONfile(@"..\..\..\database.json");
        }

        private void hideErrorLabels()
        {
            errorMessageSender.Visibility = System.Windows.Visibility.Hidden;
            errorMessageSubject.Visibility = System.Windows.Visibility.Hidden;
            errorMessageSortCode.Visibility = System.Windows.Visibility.Hidden;
            errorMessageNatureOfIncident.Visibility = System.Windows.Visibility.Hidden;
            errorMessageBody.Visibility = System.Windows.Visibility.Hidden;
        }

        private void hideInputFields()
        {
            lblSender.Visibility = System.Windows.Visibility.Hidden;
            lblSubject.Visibility = System.Windows.Visibility.Hidden;
            lblSortcode.Visibility = System.Windows.Visibility.Hidden;
            lblNatureOfIncident.Visibility = System.Windows.Visibility.Hidden;
            lblMessageText.Visibility = System.Windows.Visibility.Hidden;
            lblProccessedMessageText.Visibility = System.Windows.Visibility.Hidden;

            tbSender.Visibility = System.Windows.Visibility.Hidden;
            tbSubject.Visibility = System.Windows.Visibility.Hidden;
            tbSortCode.Visibility = System.Windows.Visibility.Hidden;
            natureOfIncident.Visibility = System.Windows.Visibility.Hidden;
            tbMessage.Visibility = System.Windows.Visibility.Hidden;
            tbProcessedMessage.Visibility = System.Windows.Visibility.Hidden;
        }

        private void fillInDropbox()
        {
            foreach (Message.messageType messageType in Enum.GetValues(typeof(Message.messageType)))
            {
                msgType.Items.Add(messageType);
            }

            foreach (SignificantIncidentReports.natureOfIncident incident in Enum.GetValues(typeof(SignificantIncidentReports.natureOfIncident)))
            {
                natureOfIncident.Items.Add(incident);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            bool validationStatus = false;
            Message message = null;

            hideErrorLabels();

            if (msgType.SelectedIndex == -1)
            {
                MessageBox.Show("You haven't selected a message type.");
                return;
            }
            else
            {
                string messageInput = constructInput().ToString();
                validationStatus = service.categoriseAndValidate(ref message, messageInput);
            }

            if (validationStatus)
            {
                service.saveMessage(message);
                tbProcessedMessage.Text = message.MessageBody;

                if (typeof(Tweets) == message.GetType())
                {
                    UpdateTrendingList();
                }

                // If the message is from the input file, remove it from the list of unprocessed messages.
                if (messageFromInputFile)
                {
                    if (messageFromInputFileCounter > service.getInputFileSize())
                    {
                        service.removeMessageFromQueue(messageFromInputFileCounter - service.getInputFileSize());
                    }
                    else
                    {
                        service.removeMessageFromQueue(messageFromInputFileCounter);
                    }
                    messageFromInputFile = false;
                    lblUnprocessedMessages.Content = "There are " + service.InputMessages().Count + " unprocessed messages";
                }

                MessageBox.Show("Message saved.");
                resetInputFields();
            }
            else
            {
                if(Message.MessageCounter != 0)
                {
                    Message.MessageCounter = Message.MessageCounter - 1;
                }
                message = null;
                return;
            }
        }

        private void msgType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            resetInputFields();
        }

        // Resets the input fields to be empty and with default values.
        private void resetInputFields()
        {
            if (msgType.SelectedIndex != -1)
            {
                tbSender.IsEnabled = true;
                tbSender.Visibility = System.Windows.Visibility.Visible;
                tbMessage.IsEnabled = true;
                tbMessage.Visibility = System.Windows.Visibility.Visible;
                tbProcessedMessage.Visibility = System.Windows.Visibility.Visible;

                lblSender.IsEnabled = true;
                lblSender.Visibility = System.Windows.Visibility.Visible;
                lblMessageText.IsEnabled = true;
                lblMessageText.Visibility = System.Windows.Visibility.Visible;
                lblProccessedMessageText.IsEnabled = true;
                lblProccessedMessageText.Visibility = System.Windows.Visibility.Visible;

                tbSender.Text = "";
                tbMessage.Text = "";
                tbSubject.Text = "";
                tbSortCode.Text = "";
                natureOfIncident.Text = "";
                tbProcessedMessage.Text = "";

                hideErrorLabels();

                if (msgType.SelectedItem.Equals(Message.messageType.Standard_email_message) || msgType.SelectedItem.Equals(Message.messageType.Significant_incident_report))
                {
                    tbSubject.IsEnabled = true;
                    tbSubject.Visibility = System.Windows.Visibility.Visible;
                    lblSubject.IsEnabled = true;
                    lblSubject.Visibility = System.Windows.Visibility.Visible;
                    if (msgType.SelectedItem.Equals(Message.messageType.Significant_incident_report))
                    {
                        tbSortCode.IsEnabled = true;
                        tbSortCode.Visibility = System.Windows.Visibility.Visible;
                        natureOfIncident.IsEnabled = true;
                        natureOfIncident.Visibility = System.Windows.Visibility.Visible;
                        lblSortcode.IsEnabled = true;
                        lblSortcode.Visibility = System.Windows.Visibility.Visible;
                        lblNatureOfIncident.IsEnabled = true;
                        lblNatureOfIncident.Visibility = System.Windows.Visibility.Visible;
                        natureOfIncident.SelectedValue = SignificantIncidentReports.natureOfIncident.None;
                    }
                    else
                    {

                        tbSortCode.IsEnabled = false;
                        tbSortCode.Visibility = System.Windows.Visibility.Hidden;
                        natureOfIncident.IsEnabled = false;
                        natureOfIncident.Visibility = System.Windows.Visibility.Hidden;
                        lblSortcode.IsEnabled = false;
                        lblSortcode.Visibility = System.Windows.Visibility.Hidden;
                        lblNatureOfIncident.IsEnabled = false;
                        lblNatureOfIncident.Visibility = System.Windows.Visibility.Hidden;
                        natureOfIncident.Text = "";
                    }
                }
                else
                {
                    tbSubject.IsEnabled = false;
                    tbSubject.Visibility = System.Windows.Visibility.Hidden;
                    tbSortCode.IsEnabled = false;
                    tbSortCode.Visibility = System.Windows.Visibility.Hidden;
                    natureOfIncident.IsEnabled = false;
                    natureOfIncident.Visibility = System.Windows.Visibility.Hidden;

                    lblSubject.IsEnabled = false;
                    lblSubject.Visibility = System.Windows.Visibility.Hidden;
                    lblSortcode.IsEnabled = false;
                    lblSortcode.Visibility = System.Windows.Visibility.Hidden;
                    lblNatureOfIncident.IsEnabled = false;
                    lblNatureOfIncident.Visibility = System.Windows.Visibility.Hidden;
                }
            }
        }

        // Set the error message for the sender field.
        public string ErrorMessageSender
        {
            set
            {
                errorMessageSender.Visibility = System.Windows.Visibility.Visible;
                errorMessageSender.Content = value;
            }
        }

        // Set the error message for the message body field.
        public string ErrorMessageBody
        {
            set
            {
                errorMessageBody.Visibility = System.Windows.Visibility.Visible;
                errorMessageBody.Content = value;
            }
        }

        // Set the error message for the subject field.
        public string ErrorMessageSubject
        {
            set
            {
                errorMessageSubject.Visibility = System.Windows.Visibility.Visible;
                errorMessageSubject.Content = value;
            }
        }

        // Set the error message for the sort code field.
        public string ErrorMessageSortCode
        {
            set
            {
                errorMessageSortCode.Visibility = System.Windows.Visibility.Visible;
                errorMessageSortCode.Content = value;
            }
        }

        // Set the error message for the nature of incident field.
        public string ErrorMessageNatureOfIncident
        {
            set
            {
                errorMessageNatureOfIncident.Visibility = System.Windows.Visibility.Visible;
                errorMessageNatureOfIncident.Content = value;
            }
        }

        // Access the value for the sender field.
        public string MessageSender
        {
            get
            {
                return this.tbSender.Text;
            }
        }

        // Access the value for the message body field.
        public string MessageBody
        {
            get
            {
                return this.tbMessage.Text;
            }
        }

        // Access the value for the subject field.
        public string MessageSubject
        {
            get
            {
                return this.tbSubject.Text;
            }
        }

        // Access the value for the sort code field.
        public string SortCode
        {
            get
            {
                return this.tbSortCode.Text;
            }
        }

        // Access the value for the nature of incident field.
        public SignificantIncidentReports.natureOfIncident Incident
        {
            get
            {
                return (SignificantIncidentReports.natureOfIncident)this.natureOfIncident.SelectedItem;
            }
        }

        // Updates the trending list for tweets.
        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            UpdateTrendingList();
        }

        private void UpdateTrendingList()
        {
            List<Label> trendingLabels = new List<Label>();
            trendingLabels.Add(lblTrending1);
            trendingLabels.Add(lblTrending2);
            trendingLabels.Add(lblTrending3);
            trendingLabels.Add(lblTrending4);
            trendingLabels.Add(lblTrending5);

            if (service.trendingTopics().Count == 0)
            {
                lblTrending1.Content = "No trending topics.";
            }
            else
            {
                int limit = Math.Min(5, service.trendingTopics().Count);
                int i = 0;
                
                foreach (KeyValuePair<string, int> hashtag in service.trendingTopics().OrderByDescending(key => key.Value))
                {
                    if (i < limit)
                    {
                        trendingLabels[i].Content = i + 1 + ". " + hashtag.Key + " - " + hashtag.Value;
                        i++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        // Builds a string of all inputs that is taken by the service.
        private StringBuilder constructInput()
        {
            StringBuilder input = new StringBuilder();

            switch (msgType.SelectedItem)
            {
                case Message.messageType.SMS:
                    input.Append(Message.messageType.SMS);
                    input.Append("¡");
                    input.Append(MessageSender);
                    input.Append("¡");
                    input.Append(MessageBody);
                    break;
                case Message.messageType.Tweet:
                    input.Append(Message.messageType.Tweet);
                    input.Append("¡");
                    input.Append(MessageSender);
                    input.Append("¡");
                    input.Append(MessageBody);
                    break;
                case Message.messageType.Standard_email_message:
                    input.Append(Message.messageType.Standard_email_message);
                    input.Append("¡");
                    input.Append(MessageSender);
                    input.Append("¡");
                    input.Append(MessageSubject);
                    input.Append("¡");
                    input.Append(MessageBody);
                    break;
                case Message.messageType.Significant_incident_report:
                    input.Append(Message.messageType.Significant_incident_report);
                    input.Append("¡");
                    input.Append(MessageSender);
                    input.Append("¡");
                    input.Append(MessageSubject);
                    input.Append("¡");
                    input.Append(SortCode);
                    input.Append("¡");
                    input.Append(Incident);
                    input.Append("¡");
                    input.Append(MessageBody);
                    break;
            }

            return input;
        }

        // Loading and reading an input text file with messages.
        private void btnLoadFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt";
            openFileDialog.ShowDialog();

            if (openFileDialog.FileName != "")
            {
                service.readInputFile(openFileDialog.FileName);
                lblUnprocessedMessages.Content = "There are " + service.InputMessages().Count +" unprocessed messages";
                lblUnprocessedMessages.Visibility = Visibility.Visible;
            }
        }

        // Load a message from the list of unprocessed messages. These messages are coming from the input file.
        private void btnLoadMessage_Click(object sender, RoutedEventArgs e)
        {
            if (service.getInputFileSize() != 0)
            {
                // Users should not be able to change the message type for a message from an input file.
                msgType.IsEnabled = false;
                messageFromInputFileCounter += 1;
                messageFromInputFile = true;

                string[] line = service.getNextInputMessage(messageFromInputFileCounter).Item2;
                messageFromInputFileCounter = service.getNextInputMessage(messageFromInputFileCounter).Item1;

                switch ((Message.messageType)Enum.Parse(typeof(Message.messageType), line[0]))
                {
                    case Message.messageType.SMS:
                        msgType.SelectedItem = Message.messageType.SMS;
                        tbSender.Text = line[1];
                        tbMessage.Text = line[2];
                        break;
                    case Message.messageType.Tweet:
                        msgType.SelectedItem = Message.messageType.Tweet;
                        tbSender.Text = line[1];
                        tbMessage.Text = line[2];
                        break;
                    case Message.messageType.Standard_email_message:
                        msgType.SelectedItem = Message.messageType.Standard_email_message;
                        tbSender.Text = line[1];
                        tbSubject.Text = line[2];
                        tbMessage.Text = line[3];
                        break;
                    case Message.messageType.Significant_incident_report:
                        msgType.SelectedItem = Message.messageType.Significant_incident_report;
                        tbSender.Text = line[1];
                        tbSubject.Text = line[2];
                        tbSortCode.Text = line[3];
                        Enum.TryParse<SignificantIncidentReports.natureOfIncident>(line[4], out SignificantIncidentReports.natureOfIncident incidentType);
                        natureOfIncident.SelectedItem = incidentType;
                        tbMessage.Text = line[5];
                        break;
                }
            }
            else
            {
                MessageBox.Show("You haven't selected an input file.");
            }
        }

        private void btnNewMessage_Click(object sender, RoutedEventArgs e)
        {
            msgType.IsEnabled = true;
            messageFromInputFile = false;
            resetInputFields();
        }

        // Opens a window with all processed messages until now.
        private void btnHistory_Click(object sender, RoutedEventArgs e)
        {
            if (!Application.Current.Windows.OfType<History>().Any())
            {
                History historyWindow = new History();
                historyWindow.Show();
            }
        }

        // Opens the quarantined list of URLs.
        private void btnQuarantine_Click(object sender, RoutedEventArgs e)
        {
            if (!Application.Current.Windows.OfType<QuarantineList>().Any())
            {
                QuarantineList quarantineWindow = new QuarantineList();
                quarantineWindow.Show();
            }
        }

        // Opens the list of Tweet mentions.
        private void btnMentions_Click(object sender, RoutedEventArgs e)
        {
            if (!Application.Current.Windows.OfType<Mentions>().Any())
            {
                Mentions mentionsWindow = new Mentions();
                mentionsWindow.Show();
            }
        }

        // Open the list of SIR list.
        private void btnSIR_Click(object sender, RoutedEventArgs e)
        {
            if (!Application.Current.Windows.OfType<SIRList>().Any())
            {
                SIRList sirWindow = new SIRList();
                sirWindow.Show();
            }
        }
    }
}
