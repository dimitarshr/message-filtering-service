﻿/*
 * Author: 40201757
 * Description: The QuarantineList class is part of the GUI.
 *              From here the user can see all quarantine URLs from emails.
 * Date last modified: 12/11/2018
 */
using System;
using System.Collections.Generic;
using System.Windows;
using Napier_Bank_40201757.Services;

namespace Napier_Bank_40201757
{
    public partial class QuarantineList : Window
    {
        private Service service = Service.Instance;

        public QuarantineList()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // When the window is loaded, all quarantined URLs are read and written to the data grid.
            foreach (KeyValuePair<string, List<string>> m in service.AllQuarantineURLs)
            {
                foreach(string url in m.Value)
                {
                    allURLsGrid.Items.Add(new Tuple<string, string>(m.Key, url));
                }
            }
        }
    }
}
