﻿/*
 * Author: 40201757
 * Description: The DataHandler class consists of all the associated properties required to store data.
 * Date last modified: 12/11/2018
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Napier_Bank_40201757.Messages;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Napier_Bank_40201757.Services
{
    public class DataHandler
    {
        // Data structures storing the data at run time.
        private static DataHandler instance;
        private Dictionary<string, string> abbreviations = new Dictionary<string, string>();
        private Dictionary<string, int> hashtags = new Dictionary<string, int>();
        private Dictionary<string, Message> allMessages = new Dictionary<string, Message>();
        private Dictionary<string, List<string>> allQuarantineURLs = new Dictionary<string, List<string>>();
        private Dictionary<string, List<string>> allMentions = new Dictionary<string, List<string>>();
        private Dictionary<string, Message> SIRs = new Dictionary<string, Message>();
        private Dictionary<int, string[]> inputMessages = new Dictionary<int, string[]>();
        private int inputMessagesSize = 0;

        private DataHandler() { }

        // The datahandler class is using the Singleton pattern.
        public static DataHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DataHandler();
                }
                return instance;
            }
        }

        // Sets and gets the abbreviations.
        public Dictionary<string, string> AllAbbreviations
        {
            get
            {
                if (abbreviations.Count() == 0)
                {
                    readAbbreviations();
                }
                return abbreviations;
            }
        }

        private void readAbbreviations()
        {
            using (var reader = new StreamReader(@"..\..\..\textwords.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    abbreviations.Add(values[0], values[1]);
                }
            }
        }

        // Returns all the messages.
        public Dictionary<string, Message> AllMessages
        {
            get
            {
                return allMessages;
            }
        }

        // Returns the SIR list.
        public Dictionary<string, Message> AllSIRs
        {
            get
            {
                return SIRs;
            }
        }

        // Returns the quarantined URLs list.
        public Dictionary<string, List<string>> AllQuarantineURLs
        {
            get
            {
                return allQuarantineURLs;
            }
        }

        // Returns the all mentions list.
        public Dictionary<string, List<string>> AllMentions
        {
            get
            {
                return allMentions;
            }
        }

        // Writes a message to a JSON.
        public void writeToJSONFile(Message message)
        {
            allMessages.Add(message.MessageID, message);
            using (StreamWriter file = new StreamWriter(@"..\..\..\database.json", false))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Formatting = Formatting.Indented;
                serializer.Serialize(textWriter: file, value: allMessages.Values);
            }
        }

        // Reads from a JSON file.
        public void readFromJSONFile(string filePath)
        {
            dynamic array;
            using (StreamReader r = new StreamReader(filePath))
            {
                string json = r.ReadToEnd();
                array = JsonConvert.DeserializeObject(json);

            }

            if (((JArray)array) != null)
            {
                foreach (var m in array)
                {
                    Message message = null;
                    switch ((int)m.MessageType)
                    {
                        case (int)Message.messageType.SMS:
                            message = new SMSMessage
                            {
                                Sender = m.Sender,
                                MessageBody = m.MessageBody,
                                MessageBodyRaw = m.MessageBodyRaw
                            };
                            break;
                        case (int)Message.messageType.Tweet:
                            message = new Tweets
                            {
                                Sender = m.Sender,
                                MessageBody = m.MessageBody,
                                MessageBodyRaw = m.MessageBodyRaw
                            };
                            findHashtags(message.MessageBody);
                            findMentions(message);
                            break;
                        case (int)Message.messageType.Standard_email_message:
                            message = new EmailMessages
                            {
                                Sender = m.Sender,
                                Subject = m.Subject,
                                MessageBody = m.MessageBody,
                                MessageBodyRaw = m.MessageBodyRaw
                            };
                            findURLs(message);
                            break;
                        case (int)Message.messageType.Significant_incident_report:
                            message = new SignificantIncidentReports
                            {
                                Sender = m.Sender,
                                Subject = m.Subject,
                                MessageBody = m.MessageBody,
                                SortCode = m.SortCode,
                                Incident = m.Incident,
                                MessageBodyRaw = m.MessageBodyRaw
                            };
                            findURLs(message);
                            addSIR(message);
                            break;
                    }
                    allMessages.Add(message.MessageID, message);
                }
            }
        }

        private void findHashtags(string messageBody)
        {
            foreach (string token in messageBody.Trim().Split(' '))
            {
                if (token[0] == '#')
                {
                    addHashtag(token);
                }
            }
        }

        private void findURLs(Message message)
        {
            foreach (string token in message.MessageBodyRaw.Trim().Split(' '))
            {
                // Checks if a string is a URL.
                var macth = Regex.Match(token.Trim(new Char[] { ' ', ',', '.', ';', '<', '>', '(', ')', '{', '}' }), @"^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-zA-Z0-9]+([\-\.]{1}[a-zA-Z0-9]+)*\.[a-zA-Z]{2,5}(:[0-9]{1,5})?(\/.*)?$", RegexOptions.IgnoreCase);
                if (macth.Success)
                { 
                    addQuarantineURL(message.MessageID, token);
                }
            }
        }

        private void findMentions(Message message)
        {
            foreach (string token in message.MessageBodyRaw.Trim().Split(' '))
            {
                if (token[0] == '@')
                {
                    addMention(token, message.MessageID);
                }
            }
        }

        public void addHashtag(string hastag)
        {
            if (hashtags.ContainsKey(hastag))
            {
                hashtags[hastag] += 1;
            }
            else
            {
                hashtags.Add(hastag, 1);
            }
        }

        public void addQuarantineURL(string messageID, string url)
        {
            if (allQuarantineURLs.ContainsKey(messageID))
            {
                allQuarantineURLs[messageID].Add(url);
            }
            else
            {
                allQuarantineURLs.Add(messageID, new List<string>() { url });
            }
        }

        public void addMention(string mention, string messageID)
        {
            if (allMentions.ContainsKey(mention))
            {
                allMentions[mention].Add(messageID);
            }
            else
            {
                allMentions.Add(mention, new List<string>() { messageID });
            }
        }

        public void addSIR(Message message)
        {
            if (SIRs.ContainsKey(message.MessageID))
            {
                SIRs[message.MessageID] = message;
            }
            else
            {
                SIRs.Add(message.MessageID, message);
            }
        }

        public Dictionary<string, int> getTrendingHashtags()
        {
            return hashtags;
        }

        // Returns the next input message in the queue that has been loaded from a txt file.
        public Tuple<int, string[]> GetInputMessage(int key)
        {
            // It will go over the list until it find the next message.
            while (!inputMessages.ContainsKey(key))
            {
                key += 1;
                // If we go beyond the max key, it will be reset.
                if (key > inputMessagesSize)
                {
                    key = key - inputMessagesSize - 1;
                }
            }
            return Tuple.Create(key, inputMessages[key]);
        }

        public void removeInputMessage(int key)
        {
            inputMessages.Remove(key);
        }

        public void addInputMessage(int lineNum, string[] input)
        {
            inputMessages.Add(lineNum, input);
            inputMessagesSize += 1;
        }

        public Dictionary<int, string[]> InputMessages
        {
            get
            {
                return inputMessages;
            }
        }
    }
}
