﻿/*
 * Author: 40201757
 * Description: The Services class consists of all the associated properties required to connect make the UI working.
 * Date last modified: 12/11/2018
 */
using System;
using System.Text;
using Napier_Bank_40201757.Messages;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.IO;

namespace Napier_Bank_40201757.Services
{
    public class Service
    {
        private static Service instance;
        private DataHandler dataHandler = DataHandler.Instance;
        private static MainWindow UI = null;
        private Service() {}
        private int inputFileSize = 0;

        // The service class is using the Singleton pattern.
        public static Service Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Service();
                }
                return instance;
            }
        }

        // It has a UI connection to the main window so that it can feed back any error messages.
        public void addUIConnection(MainWindow mainWindow)
        {
            // This makes sure that only the Main Window is connected as there we have the error messages.
            if (UI == null)
            {
                UI = mainWindow;
            }
        }

        // Loads the JSON file into the local data handler.
        public void loadJSONfile(string filePath)
        {
            dataHandler.readFromJSONFile(filePath);
        }

        // Takes the user input as an array of strings and validates it.
        public bool validateInput(Message message, string [] input)
        {
            bool incorrectData = false;
            string[] x = input;

            try
            {
                message.MessageBodyRaw = input[input.Length-1];
            }
            catch (Exception except)
            {
                UI.ErrorMessageBody = except.Message;
                incorrectData = true;
            }

            try
            {
                message.Sender = input[1];
            }
            catch (Exception except)
            {
                UI.ErrorMessageSender = except.Message;
                incorrectData = true;
            }

            if (message.GetType() == typeof(EmailMessages))
            {
                try
                {
                    ((EmailMessages)message).Subject = input[2];
                }
                catch (Exception except)
                {
                    UI.ErrorMessageSubject = except.Message;
                    incorrectData = true;
                }
            }
            else if (message.GetType() == typeof(SignificantIncidentReports))
            {
                try
                {
                    ((SignificantIncidentReports)message).Subject = input[2];
                }
                catch (Exception except)
                {
                    UI.ErrorMessageSubject = except.Message;
                    incorrectData = true;
                }

                try
                {
                    ((SignificantIncidentReports)message).SortCode = input[3];
                }
                catch (Exception except)
                {
                    UI.ErrorMessageSortCode = except.Message;
                    incorrectData = true;
                }


                try
                {
                    ((SignificantIncidentReports)message).Incident = (SignificantIncidentReports.natureOfIncident)Enum.Parse(typeof(SignificantIncidentReports.natureOfIncident), input[4]);
                }
                catch (Exception except)
                {
                    UI.ErrorMessageNatureOfIncident = except.Message;
                    incorrectData = true;
                }
            }

            // If at any point there was invalid data, false is returns as the validation has failed.
            if (incorrectData)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        // Processes the message and writes it ot a JSON file.
        public void saveMessage(Message message)
        {
            processMessage(message);

            if(message.GetType() == typeof(SignificantIncidentReports))
            {
                dataHandler.addSIR(message);
            }

            dataHandler.writeToJSONFile(message);
        }

        // Processes the message based on the user requirements.
        private void processMessage(Message message)
        {
            StringBuilder extendedMessageBody = new StringBuilder("");

            foreach (string token in message.MessageBodyRaw.Trim().Split(' '))
            {
                if (message.GetType() == typeof(SMSMessage) || message.GetType() == typeof(Tweets))
                {
                    extendedMessageBody.Append(token + " ");
                    if (dataHandler.AllAbbreviations.ContainsKey(token))
                    {
                        string abb = "<" + dataHandler.AllAbbreviations[token] + ">";
                        extendedMessageBody.Append(abb + " ");
                    }

                    if (message.GetType() == typeof(Tweets))
                    {
                        if (token[0] == '#')
                        {
                            dataHandler.addHashtag(token);
                        }

                        if (token[0] == '@')
                        {
                            dataHandler.addMention(token, message.MessageID);
                        }
                    }
                }
                else if (message.GetType() == typeof(EmailMessages) || message.GetType() == typeof(SignificantIncidentReports))
                {
                    // Checks for URLs.
                    var macth = Regex.Match(token.Trim(new Char[] { ' ', ',', '.', ';', '<', '>', '(', ')', '{', '}' }), @"^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-zA-Z0-9]+([\-\.]{1}[a-zA-Z0-9]+)*\.[a-zA-Z]{2,5}(:[0-9]{1,5})?(\/.*)?$", RegexOptions.IgnoreCase);
                    if (macth.Success)
                    {
                        extendedMessageBody.Append("<URL Quarantined>" + " ");
                        dataHandler.addQuarantineURL(message.MessageID, token);
                    }
                    else
                    {
                        extendedMessageBody.Append(token + " ");
                    }
                }
            }
            message.MessageBody = extendedMessageBody.ToString().Trim();
        }

        // Gets the trending list.
        public Dictionary<string, int> trendingTopics()
        {
            return dataHandler.getTrendingHashtags();
        }

        // The main method used by the UI. It categorise and validates the user input.
        public bool categoriseAndValidate(ref Message message, string input)
        {
            bool validationStatus = false;

            switch ((Message.messageType)Enum.Parse(typeof(Message.messageType), input.Split('¡')[0]))
            {
                case Message.messageType.SMS:
                    message = new SMSMessage();
                    validationStatus = validateInput(message, input.Split('¡'));
                    break;
                case Message.messageType.Tweet:
                    message = new Tweets();
                    validationStatus = validateInput(message, input.Split('¡'));
                    break;
                case Message.messageType.Standard_email_message:
                    message = new EmailMessages();
                    validationStatus = validateInput(message, input.Split('¡'));
                    break;
                case Message.messageType.Significant_incident_report:
                    message = new SignificantIncidentReports();
                    validationStatus = validateInput(message, input.Split('¡'));
                    break;
            }

            return validationStatus;
        }

        // Read the input file/
        public void readInputFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName, Encoding.Default);
            string line = "";
            inputFileSize = 0;
            while (line != null)
            {
                line = sr.ReadLine();
                if (line != null)
                {
                    inputFileSize += 1;
                    char del = '¡';
                    dataHandler.addInputMessage(inputFileSize, line.Split(del));
                }
            }
        }

        // Returns all the messages.
        public Dictionary<string, Message> AllMessages
        {
            get
            {
                return dataHandler.AllMessages;
            }
        }

        // Returns the all mentions list.
        public Dictionary<string, List<string>> AllMentions
        {
            get
            {
                return dataHandler.AllMentions;
            }
        }

        // Returns the quarantined URLs list.
        public Dictionary<string, List<string>> AllQuarantineURLs
        {
            get
            {
                return dataHandler.AllQuarantineURLs;
            }
        }

        // Returns the SIR list.
        public Dictionary<string, Message> AllSIRs
        {
            get
            {
                return dataHandler.AllSIRs;
            }
        }

        // Returns the next message from the input file.
        public Tuple<int, string[]> getNextInputMessage(int key)
        {
            return dataHandler.GetInputMessage(key);
        }

        // Removes a message once it has been saved.
        public void removeMessageFromQueue(int key)
        {
            dataHandler.removeInputMessage(key);
        }

        // Returns all messages that have been read from a file.
        public Dictionary<int, string[]> InputMessages()
        {
            return dataHandler.InputMessages;
        }

        // Gets the size of the input file.
        public int getInputFileSize()
        {
            return inputFileSize;
        }
    }
}
