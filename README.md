# Message Filtering Service
The aim of this service is to validate, sanitize and categorise incoming messages to Napier Bank in the form of **SMS text messages**, **emails** and **Tweets**. Please note, this read me file covers only the basics of the project. For more information please refer to the project's report.

# Application
All messages have a **message ID** (Message-type “S”,”E” or “T” followed by 9 numeric characters, e.g. “E1234567701”), followed by the **body** of the message. The service is capable of handling 3 types of messages that could be sent through the UI:

* SMS messages - SMS messages comprise of a **sender** in the form of an international telephone number followed by the **message text** which is a maximum of 140 characters long. The message text could be simple text but may also contain embedded “textspeak abbreviations”. Details of the textspeak abbreviations that may be embedded are supplied in the form of a CSV file.
* Email messages - Email messages comprise of a **sender** in the form of a standard email address, e.g. john.smith@example.org, followed by a 20 character **subject** and the **message text** which is a maximum of 1028 characters long. The message text could be simple text but may also contain embedded hyperlinks in the form of standard URLs e.g. https://www.google.com.
* Tweets - Tweets comprise of **sender** in the form of a Twitter ID: “@” followed by a maximum of 15 characters (e.g. @JohnSmith) and the tweet text which is a maximum of 140 characters long. In addition to ordinary text the Tweets may also contain any of the following:
	* textspeak abbreviations (as in SMS above);
	* hashtags  - strings of characters preceded by a ‘#’ sign that are used to group posts by topic, such as #Hello, #World;
	* Twitter IDs;

# User Interface
The User Interface (UI) is a simple WPF input form. The functionalities covered in the UI include:

* Sending different types of messages;
* Input and saving a set of messages from an input file (further details in the report);
* Reviewing the history of all sent messages;
* Reviewing any quarantined URLs;
* Reviewing any Tweeter mentions;
* Reviewing any treding Tweeter topics mentioned with the *#* sign;
* Reviewing any Significant Incident Reports;

# Testing
The prototype has been thoroughly tested using:

* Unit tests (*Microsoft.VisualStudio.TestTools.UnitTesting*);
* Exploratory testing;
* Performance testing;

Further details on the testing could be found in the project's report.